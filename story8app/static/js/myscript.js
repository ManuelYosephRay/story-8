$(document).ready(function() {
    $('.btn-dark').click(function() {
      $.get('/jsonreq/' + $('.searchbar').val(), function(result) {
        result.items.forEach(function(book, index) {
          $('#content').append(`
            <tr>
            <th scope="row">${index+1}</th>
            <td><img src="${book.volumeInfo.imageLinks.smallThumbnail}" alt=""></td>
            <td>${book.volumeInfo.title}</td>
            <td>${book.volumeInfo.authors.join(', ')}</td>
            <td>${book.volumeInfo.publisher}</td>
            <td>${book.volumeInfo.publishedDate}</td>
            <td class="text-center">
            </td>
            </tr>
            `)
          })
        })
      })
  
  })
  

  