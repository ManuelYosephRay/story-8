from django.shortcuts import render, redirect
from django.http import JsonResponse
import json
import requests

def book_func(request):
    return render(request, 'story8.html')

def jsonreq_func(request, book):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + book)
    return JsonResponse(response.json())
