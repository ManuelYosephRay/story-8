from django.conf.urls import url, include
from django.urls import path
from . import views

app_name = 'story8'
urlpatterns = [
    path('', views.book_func, name = 'book_func'),
    path('jsonreq/<str:book>', views.jsonreq_func, name = 'jsonreq_func')
]
