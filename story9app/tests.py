from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from story9app.views import log_in, log_out, account
from django.contrib.auth.models import User
# Create your tests here.

class UnitTestStory9(TestCase):

	@classmethod
	def setUpTestData(cls):
		cls.u1 = User.objects.create_user(username='testuser', password='testpassword')

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)
	
	# def test_loginpage_url_is_exist_use_login_template(self):
	# 	response = Client().get('/login/')
	# 	self.assertEqual(response.status_code, 200)
	# 	self.assertTemplateUsed(response, 'login.html')

	# def test_loginpage_using_login_function(self):
	# 	response = resolve('/login/')
	# 	self.assertEqual(response.func, loginpage)

	# def test_landingpage_url_is_exist_use_landing_template(self):
	# 	response = Client().get('/acc')
	# 	self.assertEqual(response.status_code, 200)
	# 	self.assertTemplateUsed(response, 'accounts.html')

	# def test_landingpage_using_landingpage_function(self):
	# 	response = resolve('/')
	# 	self.assertEqual(response.func, landingpage)

	# def test_logoutpage_redirected(self):
	# 	response = Client().get('/logout/')
	# 	self.assertEqual(response.status_code, 302)

	# def test_menupage_redirected_if_not_logged_in(self):
	# 	response = Client().get('/login/')
	# 	self.assertEqual(response.status_code, 302)

